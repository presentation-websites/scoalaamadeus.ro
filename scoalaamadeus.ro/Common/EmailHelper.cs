﻿using System;
using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace scoalaamadeus.ro.Common
{
    public class EmailHelper
    {
        public static string SendEmail(string to, string subject, string bodyText, string bodyHtml, string from, string fromName)
        {
            NameValueCollection values = new NameValueCollection();
            values.Add("apikey", "F26AD0B6709A50ECDC0D64A62269DB1C4146A5FF2EACB28DBECA2AA1DD31A0915F3C5CC6DF851346F12B5D128BADA231");
            values.Add("from", "noreply@netmirror.eu");
            values.Add("fromName", fromName);
            values.Add("to", "contact@scoalaamadeus.ro");
            values.Add("subject", subject);
            values.Add("bodyText", bodyText);
            values.Add("bodyHtml", bodyHtml);
            values.Add("isTransactional", "true");
            string address = "https://api.elasticemail.com/v2/email/send";
            using (WebClient client = new WebClient())
            {
                try
                {
                    byte[] apiResponse = client.UploadValues(address, values);
                    var encodedResponse = Encoding.UTF8.GetString(apiResponse);
                    return encodedResponse;

                }
                catch (Exception ex)
                {
                    return "Exception caught: " + ex.Message + "\n" + ex.StackTrace;
                }
            }
        }
    }
}
