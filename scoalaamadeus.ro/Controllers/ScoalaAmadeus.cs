﻿using System.IO;
using System.Web.Mvc;
using Antlr.Runtime.Misc;
using scoalaamadeus.ro.Common;
using scoalaamadeus.ro.Models;

namespace scoalaamadeus.ro.Controllers
{
    public class ScoalaAmadeusController : Controller
    {
        private string emailTemplateName = "ContactEmailTemplate";
        private string fromEmail = "noreply@netmirror.eu";
        public ScoalaAmadeusController()
        {
            base.ViewBag.Title = "Școala Amadeus";
        }

        public ActionResult Acasa()
        {
            return View();
        }

        public ActionResult Echipa()
        {
            return View();
        }

        public ActionResult MesajulDirectorului()
        {
            return View();
        }

        public ActionResult GalerieFoto()
        {
            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(string name, string email, string subject, string message)
        {
            var htmlBody = 
                this.RenderView(emailTemplateName, new EmailContactModel { Email = email, Message = message }, true);

            EmailHelper
                .SendEmail(
                "calinvlasin@yahoo.com",
                subject,
                message,
                htmlBody,
                fromEmail,
                name);

            ModelState.Clear();

            return View();
        }

        public ActionResult Pian()
        {
            return View();
        }

        public ActionResult Vioara()
        {
            return View();
        }

        public ActionResult Percutie()
        {
            return View();
        }

        public ActionResult Chitara()
        {
            return View();
        }

        public ActionResult DirijatCoral()
        {
            return View();
        }

        public ActionResult AtelierCopii()
        {
            return View();
        }

        public ActionResult TeorieSolfegiuIstoriaMuzicii()
        {
            return View();
        }

        public ActionResult TarifeSiStructuraCursurilor()
        {
            return View();
        }

        public ActionResult Canto()
        {
            return View();
        }

        public ActionResult CompozitieArmonieForme()
        {
            return View();
        }

        public ActionResult Concurs()
        {
            return View();
        }

        public ActionResult DownloadRegulament()
        {
            string virtualFilePath = "~/Content/Concursuri/1/Regulament.docx";
            return File(virtualFilePath, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(virtualFilePath));
        }

        public ActionResult DownloadFisaDeInscriere()
        {
            string virtualFilePath = "~/Content/Concursuri/1/FIŞA DE ÎNSCRIERE Cluj.docx";
            return File(virtualFilePath, System.Net.Mime.MediaTypeNames.Application.Octet, Path.GetFileName(virtualFilePath));
        }
    }
}
