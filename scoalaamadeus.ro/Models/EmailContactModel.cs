﻿namespace scoalaamadeus.ro.Models
{
    public class EmailContactModel
    {
        public string Message { get; set; }
        public string Email { get; set; }
    }
}